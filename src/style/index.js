export const primaryColor = '#4caf4f';

export const backgroundColor = 'white';

export const borderColor = 'rgb(217, 217, 217)';

export const grayedColor = 'rgba(0, 0, 0, 0.54)';

export const textColor = 'rgba(0,0,0,0.87)';

export const subheaderColor = 'rgba(0, 0, 0, 0.54)';

export const fontSize = 20;

export const shadow = {
  shadowColor: 'black',
  shadowOffset: {
    width: 0,
    height: 3,
  },
  shadowOpacity: .19,
  shadowRadius: 6,
  elevation: 4,
};